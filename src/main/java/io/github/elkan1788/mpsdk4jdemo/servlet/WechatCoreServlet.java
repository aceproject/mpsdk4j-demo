package io.github.elkan1788.mpsdk4jdemo.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.elkan1788.mpsdk4j.core.WechatDefHandler;
import io.github.elkan1788.mpsdk4j.mvc.HttpServletSupport;
import io.github.elkan1788.mpsdk4j.util.ConfigReader;
import io.github.elkan1788.mpsdk4j.vo.MPAccount;

/**
 * Servlet环境接入
 * 
 * @author 凡梦星尘(elkan1788@gmail.com)
 * @since 2.0
 */
@WebServlet(name = "wechatCoreServlet", urlPatterns = "/servlet/wechatcore.ser")
public class WechatCoreServlet extends HttpServletSupport {

    private static final long serialVersionUID = 4370883777170946295L;

    private static final Logger log = LoggerFactory.getLogger(WechatCoreServlet.class);

    private static final ConfigReader _cr = new ConfigReader("/mp.properties");

    @Override
    public void init() throws ServletException {
        log.info("====== Servlet环境 =======");
        super.init();
        MPAccount mpact = new MPAccount();
        mpact.setMpId(_cr.get("mpId"));
        mpact.setAppId(_cr.get("appId"));
        mpact.setAppSecret(_cr.get("appSecret"));
        mpact.setToken(_cr.get("token"));
        mpact.setAESKey(_cr.get("aesKey"));
        _wk.setMpAct(mpact);
        _wk.setWechatHandler(new WechatDefHandler());
    }

}
