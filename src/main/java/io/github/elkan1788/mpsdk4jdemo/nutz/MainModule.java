package io.github.elkan1788.mpsdk4jdemo.nutz;

import org.nutz.mvc.annotation.Encoding;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

/**
 * Nutz WEB环境主入口
 * 
 * @author 凡梦星尘(elkan1788@gmail.com)
 * @since 2.0
 */
@Modules(scanPackage = true)
@IocBy(type = ComboIocProvider.class, args = {
    "*anno", 
    "io.github.elkan1788.mpsdk4jdemo.nutz"
})
@Encoding(input = "UTF-8", output = "UTF-8")
public class MainModule {

}
